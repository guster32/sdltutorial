# This repo is used simply for tracking my progress of working through the tutorials

>All of the source code is copyrighted by Lazy Foo' Productions (2004-2015)
and may not be redistributed without written permission. http://lazyfoo.net/tutorials/SDL/index.php

* Install the following dependencies:
    * Install clang 9 or later (https://justiceboi.github.io/blog/install-clang-9-on-ubuntu/)
    * ```sudo apt install cmake g++ ninja-build libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev```
* Build:
    * ```mkdir -p build/default```
    * ```cd mkdir -p build/default```
    * ```cmake -GNinja ../..```
    * ```ninja```
Notes on VSCODE setup issues with lldb:
https://github.com/microsoft/vscode-cpptools/issues/5415